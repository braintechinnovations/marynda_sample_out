
<footer class="footer padding-bottom-50 ">

    {if !empty($header) && ($header == 'home')}
    <div class="menu-footer">
        <div class="container">
                    <div class="menu-footer">
                        <div class="container">
                            <div class="col-md-10 col-sm-7 padding-top-30" style="background: unset;">
                                <ul>
                                    <li><a class="border" href="{base_url("page/privacy-policy")}">Privacy Policy</a></li>
                                    <li><a class="border" href="{base_url("page/termini-e-condizioni")}">Termini e Condizioni</a></li>
                                    <li><a class="border" href="{base_url("page/about")}">Chi siamo</a></li>
                                    <li><a class="border" href="{base_url("page/reso")}">Reso</a></li>
                                    <li><a class="border" href="{base_url("page/contact-us")}">Contatti</a></li>
                                    <li><a href="{base_url("magazine/show")}">Magazine</a></li>
                                </ul>
                            </div>

                            <div class="col-md-2 col-sm-5 text-right padding-top-30" style="background: unset;">
                                <ul>
                                    <li class="face">
                                        <a class="facebook" href="{#follow_facebook#}" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="inst">
                                        <a class="instagram-social" href="{#follow_instagram#}" target="_blank">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

{*            <div class="col-md-10 col-sm-7 menu" style="margin-top: -80px; background: unset;">*}
{*                <ul>*}
{*                    <li><a class="border" href="{base_url("page/privacy-policy")}">{#link_priv#}</a></li>*}
{*                    <li><a class="border" href="{base_url("page/termini-e-condizioni")}">{#link_term#}</a></li>*}
{*                    <li><a class="border" href="{base_url("page/about")}">{#link_stor#}</a></li>*}
{*                    <li><a class="border" href="{base_url("page/contact-us")}">{#link_cont#}</a></li>*}
{*                    <li><a href="{base_url("magazine/show")}">{#link_maga#}</a></li>*}
{*                    <li><a class="border" href="{base_url("page/privacy-policy")}">Privacy Policy</a></li>*}
{*                    <li><a class="border" href="{base_url("page/termini-e-condizioni")}">Termini e Condizioni</a></li>*}
{*                    <li><a class="border" href="{base_url("page/about")}">Chi siamo</a></li>*}
{*                    <li><a class="border" href="{base_url("page/reso")}">Reso</a></li>*}
{*                    <li><a class="border" href="{base_url("page/contact-us")}">Contatti</a></li>*}
{*                    <li><a href="{base_url("magazine/show")}">Magazine</a></li>*}
{*                </ul>*}
{*            </div>*}

{*            <div class="col-md-2 col-sm-5 social text-right">*}
{*                <ul>*}
{*                    <li class="face">*}
{*                        <a class="facebook" href="{#follow_facebook#}" target="_blank">*}
{*                            <i class="fa fa-facebook"></i>*}
{*                        </a>*}
{*                    </li>*}
{*                    <li class="inst">*}
{*                        <a class="instagram-social" href="{#follow_instagram#}" target="_blank">*}
{*                            <i class="fa fa-instagram"></i>*}
{*                        </a>*}
{*                    </li>*}
{*                </ul>*}
{*            </div>*}
        </div>
    </div>
        {else}

        <div class="menu-footer">
            <div class="container">
                <div class="col-md-10 col-sm-7 menu">
                    <ul>
                        <li><a class="border" href="{base_url("page/privacy-policy")}">Privacy Policy</a></li>
                        <li><a class="border" href="{base_url("page/termini-e-condizioni")}">Termini e Condizioni</a></li>
                        <li><a class="border" href="{base_url("page/about")}">Chi siamo</a></li>
                        <li><a class="border" href="{base_url("page/reso")}">Reso</a></li>
                        <li><a class="border" href="{base_url("page/contact-us")}">Contatti</a></li>
                        <li><a href="{base_url("magazine/show")}">Magazine</a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-5 social text-right">
                    <ul>
                        <li class="face">
                            <a class="facebook" href="{#follow_facebook#}" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="inst">
                            <a class="instagram-social" href="{#follow_instagram#}" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

{*        <div class="menu-footer" style="margin-top: -80px; background: unset;">*}
{*            <div class="container">*}
{*                <div class="col-md-10 col-sm-7 padding-top-30" style="background: unset;">*}
{*                    <ul>*}
{*                        <li><a class="border" href="">{#link_home#}</a></li>*}
{*                        <li><a class="border" href="">{#link_abou#}</a></li>*}
{*                        <li><a class="border" href="">{#link_stor#}</a></li>*}
{*                        <li><a class="border" href="">{#link_cont#}</a></li>*}
{*                        <li><a  href="">{#link_blog#}</a></li>*}
{*                    </ul>*}
{*                </div>*}

{*                <div class="col-md-2 col-sm-5 text-right padding-top-30" style="background: unset;">*}
{*                    <ul>*}
{*                        <li class="face">*}
{*                            <a class="facebook" href="{#follow_facebook#}" target="_blank">*}
{*                                <i class="fa fa-facebook"></i>*}
{*                            </a>*}
{*                        </li>*}
{*                        <li class="inst">*}
{*                            <a class="instagram-social" href="{#follow_instagram#}" target="_blank">*}
{*                                <i class="fa fa-instagram"></i>*}
{*                            </a>*}
{*                        </li>*}
{*                    </ul>*}
{*                </div>*}
{*            </div>*}
{*        </div>*}
    {/if}

    <div class="container ">
        <div class="subs-footer">
            <div class="form-newsletter">

                {literal}
                <!-- Begin Mailchimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
                    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                </style>
                <div id="mc_embed_signup">
                    <form action="https://marynda.us3.list-manage.com/subscribe/post?u=986a1f4847007632cc1e20999&amp;id=d3c1bcf4fb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">

                            <input type="email" value="" name="EMAIL" class="newsletter-input form-control" id="mce-EMAIL" placeholder="{/literal}{#newsletter_email#}{literal}" required>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_986a1f4847007632cc1e20999_d3c1bcf4fb" tabindex="-1" value=""></div>
                            <div class="input-group-btn text-center padding-top-20"><input type="submit" value="{/literal}{#newsletter_subsc#}{literal}" name="subscribe" id="mc-embedded-subscribe" class="wpcf7-form-control wpcf7-submit btn btn-default"></div>
                        </div>
                    </form>
                </div>
                {/literal}

                <!--End mc_embed_signup-->

{*                <div class="input-group newsletter-group">*}
{*                    <span class="wpcf7-form-control-wrap email-632"><input type="email" name="email-632" value="" size="40" class=" newsletter-input form-control" aria-required="true" aria-invalid="false" placeholder="{#newsletter_email#}"></span><p></p>*}
{*                    <div class="input-group-btn text-center padding-top-20"><input type="submit" value="{#newsletter_subsc#}" class="wpcf7-form-control wpcf7-submit btn btn-default"><span class="ajax-loader"></span><span class="arrow-btn"></span></div>*}
{*                </div>*}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="f-content">
            <div class="f-col align-items-center">
                <p>© <a href="">Marynda</a> 2020. Engineered with <i class="fa fa-coffee"></i> by BrainTech Innovations. </p>
            </div>
            <div class="f-col">
                <a href=""><img src="{base_url('assets-shop/img/credit-card-icons.png')}" alt="" class="img-responsive"></a>
            </div>
        </div>
    </div>
</footer>
<!-- EndContent -->
</div>

<div class="whatsapp_chat_support wcs_fixed_left" id="whatsapp_supp_1">
    <div class="wcs_button wcs_button_circle">
        <span class="fa fa-whatsapp"></span>
    </div>
    <div class="wcs_button_label">
        {#chat_any_questions#}
    </div>

    <div class="wcs_popup">
        <div class="wcs_popup_close">
            <span class="fa fa-close"></span>
        </div>
        <div class="wcs_popup_header">
            <span class="fa fa-whatsapp"></span>
            <strong>{#chat_marynda_online#}</strong>

            <div class="wcs_popup_header_description">{#chat_days#}<br />{#chat_hours#}</div>
        </div>
        <div class="wcs_popup_input" data-number="{#chat_number#}" data-availability='{ "monday":["10:00-13:00", "16:00-20:00"], "tuesday":["10:00-13:00", "16:00-20:00"], "wednesday":["10:00-13:00", "16:00-20:00"], "thursday":["10:00-13:00", "16:00-20:00"], "friday":["10:00-13:00", "16:00-20:00"] , "saturday":["10:00-13:00", "16:00-20:00"]}'>
            <input type="text" placeholder="{#chat_placeholder#}" />
            <i class="fa fa-play"></i>
        </div>
        <div class="wcs_popup_avatar">
            <img src="{base_url('assets/media/logos/bollino_marynda_whatsapp.jpg')}" alt="">
        </div>
    </div>
</div>

<a href="#" class="zoa-btn scroll_top"><i class="ion-ios-arrow-up"></i></a>
<script>var HOST_URL = "{base_url()}";</script>


<script src="{base_url('assets-shop/js/jquery.js')}"></script>
<script src="{base_url('assets-shop/js/bootstrap.min.js')}"></script>
<script src="{base_url('assets-shop/js/owl.carousel.min.js')}"></script>
<script src="{base_url('assets-shop/js/slick.min.js')}"></script>
<script src="{base_url('assets-shop/js/countdown.js')}"></script>
<script src="{base_url('assets-shop/js/jquery-ui_c2b0c2990b0899f3e3e3d0791bf33206.js')}"></script>
<script src="{base_url('assets-shop/js/main.js')}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<script src="{base_url('assets/js/pages/custom/global/common-global.js')}?v={$smarty.now}"></script>

<script src="{base_url('assets/plugins/custom/moment/moment.min.js')}"></script>
<script src="{base_url('assets/plugins/custom/moment/moment-timezone-with-data.min.js')}"></script>

{if !empty($smarty.server.PATH_INFO) && $smarty.server.PATH_INFO|strstr:"/shop/cart"}
<script src="https://www.paypal.com/sdk/js?client-id={#paypal_id#}&currency={#site_currency_text#}&commit=false&debug=true&disable-funding=card"></script>
{/if}

<script src="{base_url('assets-shop/js/custom_shop.js')}?v={$smarty.now}"></script>



{if !empty($smarty.server.PATH_INFO) && $smarty.server.PATH_INFO|strstr:"/shop/returns"}
    <script src="{base_url('assets/js/pages/custom/returns/shop-returns.js')}?v={$smarty.now}"></script>
{/if}


<script src="{base_url('assets/plugins/custom/whatsapp/whatsapp-chat-support.js')}"></script>
<script>
    $('#whatsapp_supp_1').whatsappChatSupport({
        debug: false,
    });

    if ( $('.carousel').length ) {
        $('.carousel').carousel()
    }
</script>


</body>

</html>