
<!-- push menu-->
<div class="pushmenu menu-home5">
    <div class="menu-push">
        <span class="close-left js-close"><i class="ion-ios-close-empty f-40"></i></span>
        <div class="clearfix"></div>
        <form role="search" id="searchform" class="searchform" method="get" action="{base_url('shop/search')}">
            <div>
                <label class="screen-reader-text" for="q"></label>
                <input type="text" placeholder="{#search_products#}" value="" name="q" id="q" autocomplete="off">
                <input type="hidden" name="type" value="product">
                <button type="submit" id="searchsubmit"><i class="ion-ios-search-strong"></i></button>
            </div>
        </form>
        <ul class="nav-home5 js-menubar">
{*            <li class="level1 active dropdown">*}
{*                <a href="#">Home</a>*}
{*                <span class="icon-sub-menu"></span>*}
{*                <ul class="menu-level1 js-open-menu">*}
{*                    <li class="level2"><a href="#" title="">Test</a></li>*}
{*                    <li class="level2"><a href="#" title="">Test</a></li>*}
{*                </ul>*}
{*            </li>*}
            <li class="level1 active dropdown {if !empty($nav_filters->settore) && $nav_filters->settore == 'donna'}active-lateral{/if}"><a href="{base_url('../donna')}">{#menu_top_women#}</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">{#menu_top_sub_abbigliamento#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'donna'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                        <li class="level2">
                            <a href="#">{#menu_top_sub_accessori#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'accessori-donna'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/accessori-donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                        <li class="level2">
                            <a href="#">{#menu_top_sub_beauty#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'beauty-donna'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/beauty-donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                        <li class="level2">
                            <a href="#">Brand</a>
                            <ul class="menu-level-2">
                                {foreach from=$brand_settori_list key=k item=v}
                                    {if $brand_settori_list[$k]['super_settore'] == 'donna'}
                                        <li class="level2"><a href="{base_url('shop/brand')}/{$brand_settori_list[$k]['ti_slug']}/donna" title="{$brand_settori_list[$k]['ti_desc']}">{$brand_settori_list[$k]['ti_desc']}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            <li class="level1 active dropdown {if !empty($nav_filters->settore) && $nav_filters->settore == 'uomo'}active-lateral{/if}"><a href="{base_url('../uomo')}">{#menu_top_men#}</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">{#menu_top_sub_abbigliamento#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'uomo'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                        <li class="level2">
                            <a href="#">{#menu_top_sub_accessori#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'accessori-uomo'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/accessori-uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                        <li class="level2">
                            <a href="#">{#menu_top_sub_beauty#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'beauty-uomo'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/beauty-uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>

                        <li class="level2">
                            <a href="#">Brand</a>
                            <ul class="menu-level-2">
                                {foreach from=$brand_settori_list key=k item=v}
                                    {if $brand_settori_list[$k]['super_settore'] == 'uomo'}
                                        <li class="level2"><a href="{base_url('shop/brand')}/{$brand_settori_list[$k]['ti_slug']}/uomo" title="{$brand_settori_list[$k]['ti_desc']}">{$brand_settori_list[$k]['ti_desc']}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </li>
            {* <li class="level1 active dropdown {if !empty($nav_filters->settore) && $nav_filters->settore == 'food'}active-lateral{/if}"><a href="{base_url('../food')}">{#menu_top_food#}</a>
                <span class="icon-sub-menu"></span>
                <div class="menu-level1 js-open-menu">
                    <ul class="level1">
                        <li class="level2">
                            <a href="#">{#menu_top_sub_category#}</a>
                            <ul class="menu-level-2">
                                {foreach from=$super_gruppi_list key=k item=v}
                                    {if $super_gruppi_list[$k]->sr_slug == 'food'}
                                        <li class="level2"><a href="{base_url('shop/grid')}/food/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
<<<<<<< Updated upstream
            </li> *}
=======
            </li>
>>>>>>> Stashed changes
{*            <li class="level1">*}
{*                <a href="#">{#menu_top_brand#}</a>*}
{*                <span class="icon-sub-menu"></span>*}
{*                <ul class="menu-level1 js-open-menu">*}
{*                    {foreach from=$brand_list key=k item=v}*}
{*                        <li class="level2"><a href="{base_url('shop/brand')}/{$brand_list[$k]->ti_slug}" title="{$brand_list[$k]->ti_desc}">{$brand_list[$k]->ti_desc}</a></li>*}
{*                    {/foreach}*}
{*                </ul>*}
{*            </li>*}
{*            <li class="level1">*}
{*                <a href="#">{#menu_top_sale#}</a>*}
{*                <span class="icon-sub-menu"></span>*}
{*                <ul class="menu-level1 js-open-menu">*}
{*                    {foreach from=$brand_list key=k item=v}*}
{*                        <li class="level2"><a href="{base_url('shop/sale')}/{$brand_list[$k]->ti_slug}" title="{$brand_list[$k]->ti_desc}">{$brand_list[$k]->ti_desc}</a></li>*}
{*                    {/foreach}*}
{*                </ul>*}
{*            </li>*}
            <li class="level1">
                <a href="{base_url('magazine/show')}">{#menu_top_magazine#}</a>
            </li>
            <li class="level1">
                <a href="{base_url('page/about')}">Chi siamo</a>
{*                <span class="icon-sub-menu"></span>*}
{*                <ul class="menu-level1 js-open-menu">*}
{*                    <li class="level2"><a href="#" title="">Test</a></li>*}
{*                    <li class="level2"><a href="#" title="">Test</a></li>*}
{*                </ul>*}
            </li>
        </ul>
        <ul class="mobile-account">

            {if !empty($user_logged) && $user_logged->isLogged}
{*                <li class="js-user close-left"><a href="#"><i class="fa fa-shopping-bag"></i>Ordini e Profilo</a></li>*}
                <li class="js-user-logout"><a href="#"><i class="fa fa-sign-out"></i>Logout</a></li>
            {else}
                <li class="js-user mobile-login"><a href="#"><i class="fa fa-unlock-alt"></i>{#menu_login#}</a></li>
            {/if}

            <li><a href="{base_url('shop/returns/landing')}"><i class="fa fa-undo"></i>{#menu_returns_desc#}</a></li>

            <li><a href="{base_url('shop/wishlist')}"><i class="fa fa-heart"></i>{#menu_wishl#}</a></li>

        </ul>
        <h4 class="mb-title">{#menu_follo#}</h4>
        <div class="mobile-social mg-bottom-30">
            <a href="{#follow_facebook#}" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="{#follow_instagram#}" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
    </div>
</div>
<!-- end push menu-->
<!-- Push cart -->
<div class="pushmenu pushmenu-left cart-box-container">
    <div class="cart-list">
        <div class="cart-list-heading">
            <h3 class="cart-title">{#shop_cart#}</h3>
            <span class="close-left js-close"><i class="ion-ios-close-empty"></i></span>
        </div>
        <style>
            .table-content-cart{
                overflow-y:scroll;
                height:80vh;
            }
        </style>
        <div class="cart-inside">

            {if $cart_counter > 0}
                <div class="table-content-cart">
                    <table class="table cart-table table-responsive ">
{*                        <thead>*}
{*                        <tr>*}
{*                            <th class="product-thumbnail"></th>*}
{*                            <th class="product-name">{#cart_prod#}</th>*}
{*                            <th class="product-name">{#cart_pric#}</th>*}
{*                            <th class="product-name">{#cart_quan_sho#}</th>*}
{*                        </tr>*}
{*                        </thead>*}
                        <tbody id="shop-cart-content">
                        </tbody>
                    </table>
                </div>

                <div class="cart-bottom">
                    <div class="cart-button mg-top-30">
                        <button type="button" class="zoa-btn checkout initiate-ceckout btn-block" title="">{#shop_cart_go#}</button>
                    </div>
                </div>
            {else}
                <div class="table-content-cart-empty">
                    <div class="cart-bottom">
                        <p>{#shop_cart_no_products#}</p>
                        <div class="cart-button mg-top-30">
                            <a class="zoa-btn checkout" href="{base_url('shop/grid')}" title="">{#shop_cart_start_shopping#}</a>
                        </div>
                    </div>
                </div>
            {/if}


        </div>
        <!-- End cart bottom -->
    </div>
</div>
<!-- End pushcart -->
<!-- Search form -->
<div class="search-form-wrapper header-search-form">
    <div class="container">
        <div class="search-results-wrapper">
            <div class="btn-search-close">
                <i class="ion-ios-close-empty"></i>
            </div>
        </div>
        <form method="get" action="{base_url('shop/search')}" role="search" class="search-form  has-categories-select">
            <input name="q" class="search-input" type="text" value="" placeholder="{#search#}..." autocomplete="off">
            <input type="hidden" name="post_type" value="product">
            <button type="submit" id="search-btn"><i class="ion-ios-search-strong"></i></button>
        </form>
    </div>
</div>
<!-- End search form -->
<!-- Account -->
<div class="account-form-wrapper">
    <div class="container">
        <div class="search-results-wrapper">
            <div class="btn-search-close">
                <i class="ion-ios-close-empty black"></i>
            </div>
        </div>

        {if !empty($user_logged) && $user_logged->isLogged}
            <div class="account-wrapper">
                <ul class="account-tab text-center">
                    <li class="active" style="padding-left: 22px;"><a data-toggle="tab" href="#orders" class="orders-tab-show">{#profile_orders#}</a></li>
                    <li><a data-toggle="tab" href="#profile" class="profile-tab-show">{#profile_details#}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="orders" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8">
                                <form class="form-customer">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        {#profile_orders_order_date#}
                                                    </th>
                                                    <th>
                                                        {#profile_orders_order_code#}
                                                    </th>
                                                    <th>
                                                        {#profile_orders_order_payd#}
                                                    </th>
                                                    <th>
                                                        {#profile_orders_order_stat#}
                                                    </th>
                                                    <th>
                                                        {#profile_returns_button#}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="order-table-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>
                    <div id="profile" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <form method="post" class="form-customer form-register" name="form-menu-update">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_first_name#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_user_firstname" id="profile_user_firstname" disabled>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_last_name#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_user_lastname" id="profile_user_lastname" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_email#} *</p>
                                            <input type="email" class="form-control form-account" name="profile_user_email" id="profile_user_email" disabled>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_telephone#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_user_phone" id="profile_user_phone" disabled>
                                        </div>
                                    </div>

                                    <hr >

                                    <div class="row">
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <p>{#user_invoicing_address#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_invo_address" id="profile_invo_address" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <p>{#user_city#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_invo_city" id="profile_invo_city" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_state#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_invo_state" id="profile_invo_state" disabled>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact">
                                            <p>{#user_postcode#} *</p>
                                            <input type="text" class="form-control form-account editable" name="profile_invo_postcode" id="profile_invo_postcode" disabled>
                                        </div>
                                    </div>

                                    <div class="btn-button-group mg-top-30 mg-bottom-15">
                                        <button type="button" class="zoa-btn btn-login hover-white btn-block form-menu-update-enable">{#menu_edita#}</button>
                                    </div>

                                    <div class="row form-menu-update-disable-row" style="display: none;">
                                        <div class="form-group col-md-2 col-sm-2 col-xs-2 from-contact">
                                            <button type="button" class="zoa-btn btn-login hover-white btn-block form-menu-update-disable"><i class="fa fa-times"></i>{#menu_undoa#}</a></button>
                                        </div>
                                        <div class="form-group col-md-2 col-sm-2 col-xs-2 from-contact">
                                            <button type="button" class="zoa-btn btn-login hover-white btn-block form-menu-update-submit"><i class="fa fa-floppy-o"></i>{#menu_updat#}</a></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        {else}
            <div class="account-wrapper">
                <ul class="account-tab text-center">
                    <li class="active" style="padding-left: 22px;"><a data-toggle="tab" href="#login">{#register_login#}</a></li>
                    <li><a data-toggle="tab" href="#register">{#register_signup#}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="login" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <form method="post" class="form-customer form-login" name="form-menu-signin">
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">{#register_email#}</label>
                                        <input type="email" class="form-control form-account" name="fld_email" id="fld_email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword3">{#register_password#}</label>
                                        <input type="password" class="form-control form-account" name="fld_password" id="fld_password">
                                    </div>
                                    <div class="flex justify-content-between mg-30">
                                        <div class="checkbox">
                                            <input data-val="true" data-val-required="The Remember me? field is required." id="fld_remember" name="fld_remember" type="checkbox" value="true" />
                                            <input name="fld_remember" type="hidden" value="false" />
                                            <label for="fld_remember">{#register_remember#}</label>
                                        </div>
                                        <a href="#" class="text-note no-mg password-recovery-button">{#register_forgot#}</a>
                                    </div>
                                    <div class="btn-button-group mg-top-30 mg-bottom-15">
                                        <button type="button" class="zoa-btn btn-login hover-white btn-block form-menu-signin-submit">{#register_login#}</button>
                                    </div>
                                </form>

                                {*                            <span class="text-note">{#register_noaccount#} <a href="">{#register_signup#}!</a></span>*}

                                <form method="post" class="form-customer form-reset" name="form-menu-forgot" style="display: none;">
                                    <div class="form-group">
                                        <label for="exampleInputEmail5">E-mail *</label>
                                        <input type="email" class="form-control form-account" name="fld_email_forgot" id="fld_email_forgot">
                                    </div>
                                    <div class="btn-button-group mg-top-30 mg-bottom-15">
                                        <button type="button" class="zoa-btn btn-login btn-block hover-white form-menu-forgot-submit">Reset Password</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                {*                            <form method="post" class="form-customer form-reset">*}
                                {*                                <div class="form-group">*}
                                {*                                    <label for="exampleInputEmail5">E-mail *</label>*}
                                {*                                    <input type="email" class="form-control form-account" id="exampleInputEmail5">*}
                                {*                                </div>*}
                                {*                                <div class="btn-button-group mg-top-30 mg-bottom-15">*}
                                {*                                    <button type="submit" class="zoa-btn btn-login hover-white">Reset Password</button>*}
                                {*                                </div>*}
                                {*                            </form>*}
                            </div>
                        </div>
                    </div>
                    <div id="register" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <form method="post" class="form-customer form-register" name="form-menu-signup">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact left">
                                        <p>{#user_first_name#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_user_firstname" id="menu_user_firstname">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact right">
                                        <p>{#user_last_name#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_user_lastname" id="menu_user_lastname">
                                    </div>

                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact left">
                                        <p>{#user_email#} *</p>
                                        <input type="email" class="form-control form-account" name="menu_user_email" id="menu_user_email">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact right">
                                        <p>{#user_telephone#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_user_phone" id="menu_user_phone">
                                    </div>
                                    <hr >
                                    <div class="form-group">
                                        <p>{#user_invoicing_address#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_invo_address" id="menu_invo_address">
                                    </div>
                                    <div class="form-group">
                                        <p>{#user_city#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_invo_city" id="menu_invo_city">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact left">
                                        <p>{#user_state#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_invo_state" id="menu_invo_state">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6 from-contact right" style="align-content: center;">
                                        <p>{#user_postcode#} *</p>
                                        <input type="text" class="form-control form-account" name="menu_invo_postcode" id="menu_invo_postcode">
                                    </div>


                                    <style>
                                        .loader {
                                            margin: 5px auto;
                                            border: 6px solid #f3f3f3;
                                            border-radius: 50%;
                                            border-top: 6px solid #3498db;
                                            width: 40px;
                                            height: 40px;
                                            -webkit-animation: spin 1s linear infinite; /* Safari */
                                            animation: spin 1s linear infinite;
                                        }

                                        /* Safari */
                                        @-webkit-keyframes spin {
                                            0% { -webkit-transform: rotate(0deg); }
                                            100% { -webkit-transform: rotate(360deg); }
                                        }

                                        @keyframes spin {
                                            0% { transform: rotate(0deg); }
                                            100% { transform: rotate(360deg); }
                                        }
                                    </style>

                                    <div class="btn-button-group mg-top-30 mg-bottom-15">
                                        <button type="button" class="zoa-btn btn-login hover-white btn-block form-menu-signup-submit">
                                            {#register_signup#}
                                            <div class="loader loader-signup" style="display: none;"></div>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}

    </div>
    <!-- End Account -->
</div>
<div class="wrappage">
    {if !empty($header) && $header == 'home'}

    {elseif !empty($header) && $header == 'grid'}
        <header id="header" class="header-v6">
    {else}
            <header id="header" class="header-v2">
    {/if}


        {assign var="stringhetta" value=#stripe_title#|trim}
        {if !empty($stringhetta)}
            <div class="row">
                <div class="col text-center" style="background-color: {#stripe_color_background#}; color: {#stripe_color_text#}; padding-bottom: 3px; padding-top: 3px;">
                    {#stripe_title#}<strong> {#stripe_code#}</strong>
                </div>
            </div>
        {/if}
        <div class="header-center">
            <div class="container container-content">
                <div class="row flex align-items-center justify-content-between">
                    <div class="col-md-4 col-xs-4 col-sm-4 col2 hidden-lg hidden-md">
                        <div class="topbar-right">
                            <div class="element">
                                <a href="#" class="icon-pushmenu js-push-menu">
                                    <svg width="26" height="16" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 66 41" style="enable-background:new 0 0 66 41;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0 {
                                                    fill: none;
                                                    stroke: #000000;
                                                    stroke-width: 3;
                                                    stroke-linecap: round;
                                                    stroke-miterlimit: 10;
                                                }
                                            </style>
                                        <g>
                                            <line class="st0" x1="1.5" y1="1.5" x2="64.5" y2="1.5" />
                                            <line class="st0" x1="1.5" y1="20.5" x2="64.5" y2="20.5" />
                                            <line class="st0" x1="1.5" y1="39.5" x2="64.5" y2="39.5" />
                                        </g>
                                        </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-4 col-sm-4 col2 justify-content-center">
                        {if !empty($header) && $header == 'home'}
                            <a href="{base_url()}"><img src="{base_url('assets-shop/img/logo_350_black.png')}" alt="" class="img-reponsive logo-main"></a>
                        {elseif !empty($header) && $header == 'food'}
                            <a href="{base_url()}"><img src="{base_url('assets-shop/img/logo_350_food.png')}" alt="" class="img-reponsive logo-main"></a>
                        {else}
                            <a href="{base_url()}"><img src="{base_url('assets-shop/img/logo_350.png')}" alt="" class="img-reponsive logo-main"></a>
                        {/if}
                    </div>
                    <div class="col-md-10 col-xs-4 col-sm-4 col2 flex justify-content-end padding-top-10">
                        <ul class="nav navbar-nav js-menubar hidden-xs hidden-sm">
                            <li class="level1 dropdown hassub"><a href="{base_url('../donna')}" {if !empty($nav_filters->settore) && $nav_filters->settore == 'donna'}class="menu-top-active"{/if}>{#menu_top_women#}</a>
                                <span class="plus js-plus-icon"></span>
                                <div class="menu-level-1 dropdown-menu style2">
                                    <ul class="level1">
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_abbigliamento#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'donna'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_accessori#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'accessori-donna'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/accessori-donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_beauty#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'beauty-donna'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/beauty-donna/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">Brand</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$brand_settori_list key=k item=v}
                                                    {if $brand_settori_list[$k]['super_settore'] == 'donna'}
                                                        <li class="level3"><a href="{base_url('shop/brand')}/{$brand_settori_list[$k]['ti_slug']}/donna" title="{$brand_settori_list[$k]['ti_desc']}">{$brand_settori_list[$k]['ti_desc']}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
{*                                        <li class="level2 col-2">*}
{*                                            <div class="demo-img">*}
{*                                                <a href="#"><strong>{#menu_top_sub_scarpe#}</strong></a>*}

{*                                                <a href="" class="effect-img3 plus-zoom">*}
{*                                                    <img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive">*}

{*                                                </a>*}
{*                                            </div>*}
{*                                        </li>*}
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            <li class="level1 dropdown hassub"><a href="{base_url('../uomo')}" {if !empty($nav_filters->settore) && $nav_filters->settore == 'uomo'}class="menu-top-active"{/if}>{#menu_top_men#}</a>
                                <span class="plus js-plus-icon"></span>
                                <div class="menu-level-1 dropdown-menu style2">
                                    <ul class="level1">
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_abbigliamento#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'uomo'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_accessori#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'accessori-uomo'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/accessori-uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">{#menu_top_sub_beauty#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'beauty-uomo'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/beauty-uomo/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-2">
                                            <a href="#">Brand</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$brand_settori_list key=k item=v}
                                                    {if $brand_settori_list[$k]['super_settore'] == 'uomo'}
                                                        <li class="level3"><a href="{base_url('shop/brand')}/{$brand_settori_list[$k]['ti_slug']}/uomo" title="{$brand_settori_list[$k]['ti_desc']}">{$brand_settori_list[$k]['ti_desc']}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </li>

                            {* <li class="level1 active dropdown"><a href="{base_url('../food')}" {if !empty($nav_filters->settore) && $nav_filters->settore == 'food'}class="menu-top-active"{/if}>{#menu_top_food#}</a>
                                <span class="plus js-plus-icon"></span>
                                <div class="menu-level-1 dropdown-menu style5">
                                    <ul class="level1">
                                        <li class="level2 col-6">
                                            <a href="#">{#menu_top_sub_category#}</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$super_gruppi_list key=k item=v}
                                                    {if $super_gruppi_list[$k]->sr_slug == 'food'}
                                                        <li class="level3"><a href="{base_url('shop/grid')}/food/{$super_gruppi_list[$k]->gs_slug}" title="{$super_gruppi_list[$k]->gs_desc}">{$super_gruppi_list[$k]->gs_desc}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                        <li class="level2 col-6">
                                            <a href="#">Brand</a>
                                            <ul class="menu-level-2">
                                                {foreach from=$brand_settori_list key=k item=v}
                                                    {if $brand_settori_list[$k]['sr_slug'] == 'food'}
                                                        <li class="level3"><a href="{base_url('shop/brand')}/{$brand_settori_list[$k]['ti_slug']}/food" title="{$brand_settori_list[$k]['ti_desc']}">{$brand_settori_list[$k]['ti_desc']}</a></li>
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
<<<<<<< Updated upstream
                            </li> *}
=======
                            </li>
>>>>>>> Stashed changes

{*                            <li class="level1 dropdown hassub"><a href="#">{#menu_top_brand#}</a>*}
{*                                <span class="plus js-plus-icon"></span>*}
{*                                <div class="menu-level-1 dropdown-menu style2">*}
{*                                    <ul class="level1">*}
{*                                        {math assign="divider" equation="ceil(x / y)" x=count($brand_list) y=4}*}

{*                                        {foreach from=$brand_list key=k item=v}*}
{*                                            {math assign="selector" equation="y % x" x=$divider y=$k}*}

{*                                            {if $selector == 0}*}
{*                                                {if $k != 0}</ul></li>{/if}*}

{*                                                <li class="level2 col-2">*}
{*                                                    <ul class="menu-level-2">*}
{*                                            {/if}*}
{*                                                    <li class="level3"><a href="{base_url('shop/brand')}/{$brand_list[$k]->ti_slug}" title="{$brand_list[$k]->ti_desc}">{$brand_list[$k]->ti_desc}</a></li>*}
{*                                            {if $k  == count($brand_list)}*}
{*                                                    </ul></li>*}
{*                                            {/if}*}
{*                                        {/foreach}*}
{*                                    </ul>*}
{*                                </div>*}
{*                            </li>*}
{*                            <li class="level1 dropdown hassub"><a href="#">{#menu_top_sale#}</a>*}
{*                                <span class="plus js-plus-icon"></span>*}
{*                                <div class="menu-level-1 dropdown-menu style2">*}
{*                                    <ul class="level1">*}
{*                                        {math assign="divider" equation="ceil(x / y)" x=count($brand_list) y=4}*}

{*                                        {foreach from=$brand_list key=k item=v}*}
{*                                            {math assign="selector" equation="y % x" x=$divider y=$k}*}

{*                                            {if $selector == 0}*}
{*                                                {if $k != 0}</ul></li>{/if}*}

{*                                                <li class="level2 col-2">*}
{*                                                    <ul class="menu-level-2">*}
{*                                            {/if}*}
{*                                                    <li class="level3"><a href="{base_url('shop/sale')}/{$brand_list[$k]->ti_slug}" title="{$brand_list[$k]->ti_desc}">{$brand_list[$k]->ti_desc}</a></li>*}
{*                                            {if $k  == count($brand_list)}*}
{*                                                    </ul></li>*}
{*                                            {/if}*}
{*                                        {/foreach}*}
{*                                    </ul>*}
{*                                </div>*}
{*                            </li>*}
                            <li class="level1 hassub dropdown">
                                <a href="{base_url("services/show")}">{#menu_top_services#}</a>
                                <div class="menu-level-1 dropdown-menu style3">

                                    {section name="i" loop=$nav_services}
                                        {if ($smarty.section.i.index+1) % 3 == 0 || $smarty.section.i.index == 0}
                                            <div class="row">
                                        {/if}

                                        <div class="col-3">
                                                <a href="{base_url("services/show")}">
                                                    <table style="width: 100%; margin-top: 10px;">
                                                        <tr>
                                                            <td style="width: 20%">
                                                                <img src="{base_url("pic-pages")}/{$nav_services[i]['img_path']}" alt="" style="width: 100%">
                                                            </td>
                                                            <td style="padding: 5px;">
                                                                <strong>{$nav_services[i]['pag_titl']}</strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </a>
                                        </div>

                                        {if ($smarty.section.i.index+1) % 3 == 0 || $smarty.section.i.index == count($nav_services)}
                                            </div>
                                        {/if}
                                    {/section}

                                </div>
                            </li>
{*                            <li class="level1 hassub dropdown">*}
{*                                <a href="#">{#menu_top_magazine#}</a>*}
{*                                <div class="menu-level-1 dropdown-menu style3">*}
{*                                    <div class="row">*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a href="" class="effect-img3 plus-zoom">*}
{*                                                    <img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive">*}

{*                                                </a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">MakeUp</div>*}
{*                                        </div>*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a class="effect-img3 plus-zoom" href=""><img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive"></a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">Profumi Donna</div>*}
{*                                        </div>*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a class="effect-img3 plus-zoom" href=""><img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive"></a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">Profumi Uomo</div>*}
{*                                        </div>*}
{*                                    </div>*}
{*                                </div>*}
{*                            </li>*}
{*                            <li class="level1 hassub dropdown">*}
{*                                <a href="#">{#menu_top_food#}</a>*}
{*                                <div class="menu-level-1 dropdown-menu style3">*}
{*                                    <div class="row">*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a href="" class="effect-img3 plus-zoom">*}
{*                                                    <img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive">*}

{*                                                </a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">MakeUp</div>*}
{*                                        </div>*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a class="effect-img3 plus-zoom" href=""><img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive"></a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">Profumi Donna</div>*}
{*                                        </div>*}
{*                                        <div class="cate-item col-md-4 col-sm-12">*}
{*                                            <div class="demo-img">*}
{*                                                <a class="effect-img3 plus-zoom" href=""><img src="{base_url('assets-shop/img/collection_4.jpg')}" alt="" class="img-reponsive"></a>*}
{*                                            </div>*}
{*                                            <div class="demo-text text-center">Profumi Uomo</div>*}
{*                                        </div>*}
{*                                    </div>*}
{*                                </div>*}
{*                            </li>*}
                            <li class="level1">
                                <a href="{base_url('magazine/show')}">{#menu_top_magazine#}</a>
                            </li>
                            <li class="level1 active dropdown">
                                <a href="#" class="js-user" ><span style="color: grey;">My</span>{#title#}</a>
                                <ul class="dropdown-menu menu-level-1">
                                    {if !empty($user_logged) && $user_logged->isLogged}
                                        <li class="level2"><a href="#" class="js-user" >Ordini e Profilo</a></li>
                                        <li class="level2"><a href="#" class="js-user-logout" >Logout</a></li>
                                    {else}
                                        <li class="level2"><a href="#" class="js-user" >Login</a></li>
                                    {/if}
                                    <li class="level2"><a href="{base_url('shop/returns/landing')}">{#menu_returns_desc#}</a></li>

{*                                    <li class="level2"><a href="{base_url('page/about')}" title="About Us ">{#link_abou#} </a></li>*}
{*                                    <li class="level2"><a href="{base_url('page/contact-us')}" title="Contact">{#link_cous#}</a></li>*}
                                </ul>
                            </li>
{*                            <li class="level1 active dropdown">*}
{*                                <a href="#">{#link_bout#}</a>*}
{*                                <ul class="dropdown-menu menu-level-1">*}
{*                                    <li class="level2"><a href="{base_url('page/about')}" title="About Us ">{#link_abou#} </a></li>*}
{*                                    <li class="level2"><a href="{base_url('page/contact-us')}" title="Contact">{#link_cous#}</a></li>*}
{*                                </ul>*}
{*                            </li>*}
                        </ul>
                        <div class="topbar-left">

                            <div class="element element-cart hidden-xs">
                                <a href="{base_url('shop/wishlist')}" class="zoa-icon">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\Heart.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                            <path d="M16.5,4.5 C14.8905,4.5 13.00825,6.32463215 12,7.5 C10.99175,6.32463215 9.1095,4.5 7.5,4.5 C4.651,4.5 3,6.72217984 3,9.55040872 C3,12.6834696 6,16 12,19.5 C18,16 21,12.75 21,9.75 C21,6.92177112 19.349,4.5 16.5,4.5 Z" fill="#000000" fill-rule="nonzero"/>
                                        </g>
                                    </svg><!--end::Svg Icon--></span>
                                </a>
                            </div>

                            <div class="element hidden">
                                <a href="#" class="zoa-icon icon-user js-user">

                                </a>
                            </div>

                            <div class="element">
                                <a href="#" class="zoa-icon icon-cart">
                                    <svg width="20" height="20" version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 55.4 55.4" style="enable-background:new 0 0 55.4 55.4;" xml:space="preserve">
                                            <g>
                                                <rect x="0.2" y="17.4" width="55" height="3.4" />
                                            </g>
                                        <g>
                                            <polygon points="7.1,55.4 3.4,27.8 3.4,24.1 7.3,24.1 7.3,27.6 10.5,51.6 44.9,51.6 48.1,27.6 48.1,24.1 52,24.1 52,27.9
        48.3,55.4   " />
                                        </g>
                                        <g>
                                            <path d="M14,31.4c-0.1,0-0.3,0-0.5-0.1c-1-0.2-1.6-1.3-1.4-2.3L19,1.5C19.2,0.6,20,0,20.9,0c0.1,0,0.3,0,0.4,0
        c0.5,0.1,0.9,0.4,1.2,0.9c0.3,0.4,0.4,1,0.3,1.5l-6.9,27.5C15.6,30.8,14.8,31.4,14,31.4z" />
                                        </g>
                                        <g>
                                            <path d="M41.5,31.4c-0.9,0-1.7-0.6-1.9-1.5L32.7,2.4c-0.1-0.5,0-1.1,0.3-1.5s0.7-0.7,1.2-0.8c0.1,0,0.3,0,0.4,0
        c0.9,0,1.7,0.6,1.9,1.5L43.4,29c0.1,0.5,0,1-0.2,1.5c-0.3,0.5-0.7,0.8-1.1,0.9c-0.2,0-0.3,0-0.4,0.1C41.6,31.4,41.6,31.4,41.5,31.4
        z" />
                                        </g>
                                        </svg>
                                    <span class="count cart-count">{$cart_counter}</span>
                                </a>
                            </div>
                            <div class="element element-search hidden-xs">
                                <a href="#" class="zoa-icon search-toggle">
                                    <svg width="20" height="20" version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 90 90" style="enable-background:new 0 0 90 90;" xml:space="preserve">
                                            <g>
                                                <path d="M0,39.4C0,50,4.1,59.9,11.6,67.3c7.4,7.5,17.3,11.6,27.8,11.6c9.5,0,18.5-3.4,25.7-9.5l19.8,19.7c1.2,1.2,3.1,1.2,4.2,0
        c0.6-0.6,0.9-1.3,0.9-2.1s-0.3-1.5-0.9-2.1L69.3,65.1c6.2-7.1,9.5-16.2,9.5-25.7c0-10.5-4.1-20.4-11.6-27.9C59.9,4.1,50,0,39.4,0
        C28.8,0,19,4.1,11.6,11.6S0,28.9,0,39.4z M63.1,15.8c6.3,6.3,9.8,14.7,9.8,23.6S69.4,56.7,63.1,63s-14.7,9.8-23.6,9.8
        S22.2,69.3,15.9,63C9.5,56.8,6,48.4,6,39.4s3.5-17.3,9.8-23.6S30.5,6,39.4,6S56.8,9.5,63.1,15.8z" />
                                            </g>
                                        </svg>
                                </a>
                            </div>
                            <div class="element hidden-sm hidden-xs hidden-md hidden-lg">
                                <a href="#" class="icon-pushmenu js-push-menu">
                                    <svg width="26" height="16" version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 66 41" style="enable-background:new 0 0 66 41;" xml:space="preserve">
                                        <style type="text/css">
                                            .st0 {
                                                fill: none;
                                                stroke: #000000;
                                                stroke-width: 3;
                                                stroke-linecap: round;
                                                stroke-miterlimit: 10;
                                            }
                                        </style>
                                        <g>
                                            <line class="st0" x1="1.5" y1="1.5" x2="64.5" y2="1.5" />
                                            <line class="st0" x1="1.5" y1="20.5" x2="64.5" y2="20.5" />
                                            <line class="st0" x1="1.5" y1="39.5" x2="64.5" y2="39.5" />
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- /header -->