<!-- Content -->
<div class=" container-fluid text-center breadcrumb-about">
    {if empty($settore)}
        <h1>{#shop_bread_newa#}</h1>
    {else}
        <h1>{$settore['sr_desc']}</h1>
    {/if}
    <ul class="breadcrumb">
        <li><a href="{base_url('shop')}">{#shop_bread_home#}</a></li>
        <li><a href="{base_url('shop/grid')}">{#shop_bread_shop#}</a></li>
            {if !empty($settore)}
                <li class="active"><a href="{base_url('shop/grid')}/{$settore['sr_slug']}">{$settore['sr_desc']}</a></li>
            {/if}

            {if !empty($gruppo)}
                <li class="active"><a href="{base_url('shop/grid')}/{$settore['sr_slug']}/{$gruppo['id_at']}">{$gruppo['gr_desc']}</a></li>
            {/if}

            {if !empty($brand)}
                <li><a href="#">Brand</a></li>
                <li class="active"><a href="{base_url('shop/grid')}/{$brand['ti_slug']}">{$brand['ti_desc']}</a></li>
            {/if}
    </ul>
</div>

<div class="filter-collection-left hidden-lg hidden-md">
    <a class="btn"><i class="fa fa-asterisk"></i> {#filter#}</a>
</div>

<div class="container container-content">
    <div class="shop-top">
{*        <div class="shop-element left">*}
{*            <ul class="nav nav-pills">*}
{*                <li class="active"><a data-toggle="pill" href="#all">{#show_all#|upper}</a></li>*}
{*                <li><a data-toggle="pill" href="#all">{#show_newest#|upper}</a></li>*}
{*                <li><a data-toggle="pill" href="#all">{#show_bestseller#|upper}</a></li>*}
{*            </ul>*}
{*        </div>*}
        <div class="shop-element right">
{*            {if !empty($local_gs) && count($local_gs) > 1}*}
{*                <div class="show">*}
{*                    <label>{#local_gs#}</label>*}
{*                    <select class="single-option-selector" id="gsselection">*}
{*                        <option value="no-filter">{#no_filter#}</option>*}
{*                        {section name="i" loop=$local_gs}*}
{*                            <option value="{$local_gs[i]->gs_slug}">{$local_gs[i]->gs_name}</option>*}
{*                        {/section}*}
{*                    </select>*}
{*                </div>*}
{*            {/if}*}
{*            {if !empty($local_brand) && count($local_brand) > 1}*}
{*            <div class="show">*}
{*                <label>{#local_brand#}</label>*}
{*                <select class="single-option-selector" id="brandselection">*}
{*                    <option value="no-filter">{#no_filter#}</option>*}
{*                    {section name="i" loop=$local_brand}*}
{*                        <option value="{$local_brand[i]->brand_slug}">{$local_brand[i]->brand_name}</option>*}
{*                    {/section}*}
{*                </select>*}
{*            </div>*}
{*            {/if}*}
        </div>
    </div>
    <div class="product-collection-grid product-grid ">
        <div class="row">
{*            {if (!empty($local_gs) && count($local_gs) > 1) || (!empty($local_brand) && count($local_brand) > 1) || (!empty($sale_active) && $sale_active > 1)}*}
                <div class="col-md-3 col-sm-3 col-xs-12 col-left collection-sidebar" id="filter-sidebar">
                    <div class="close-sidebar-collection hidden-lg hidden-md">
                        <span>{#filter#}</span><i class="icon_close ion-close"></i>
                    </div>

                    <div class="widget-filter">
                        <div class="pri">
                            <h4>{#price_filter#}</h4>
                            <div id = "slider-prices-active"></div>
                            <script>
                                var slider_min = {$price_min};
                                var slider_max = {$price_max};
                            </script>
                            <p class="range-p">
                                <label for = "slider-price" >Range:</label>
                                <input type = "text" id = "slider-price"></p>
                        </div>
                    </div>

                    {if !empty($sale_active) && $sale_active > 1}
                        <div class="widget-filter filter-cate filter-size">
                            <ul class="">
                                <li><a href="#" class="a-saleactive">{#sale_active#}</a></li>
                            </ul>
                        </div>
                    {/if}

                    {if !empty($local_gs) && count($local_gs) > 1}
                        <div class="widget-filter filter-cate filter-size bd-top">
                            <h3>{#local_gs#}</h3>
                            <ul class="">
                                    {section name="i" loop=$local_gs}
                                        <li><a href="#" class="a-gsselection" data-gs="{$local_gs[i]->gs_slug}">{$local_gs[i]->gs_name}<span>{$local_gs[i]->gs_coun}</span></a></li>
                                    {/section}
                            </ul>
                        </div>
                    {/if}
                    {if !empty($local_brand) && count($local_brand) > 1}
                        <div class="widget-filter filter-cate filter-size bd-top">
                            <h3>{#local_brand#}</h3>
                            <ul class="">
                                    {section name="i" loop=$local_brand}
                                        <li><a href="#" class="a-brandselection" data-brand="{$local_brand[i]->brand_slug}">{$local_brand[i]->brand_name}<span>{$local_brand[i]->brand_coun}</span></a></li>
                                    {/section}
                            </ul>
                        </div>
                    {/if}
                    {if !empty($local_size) && count($local_size) > 1}
                        <div class="widget-filter filter-cate filter-size bd-top">
                            <h3>{#local_size#}</h3>
                            <ul class="">
                                {section name="i" loop=$local_size}
                                    <li><a href="#" class="a-sizeselection" data-size="{$local_size[i]['mov_tari']}">{$local_size[i]['mov_tari']}</a></li>
                                {/section}
                            </ul>
                        </div>
                    {/if}
                </div>
{*            {/if}*}

            <div class="tab-content">
                <div id="all" class="tab-pane fade active in ">

                    {section name="i" loop=$articoli}
                        {if $articoli[i]['quant']|intval > 0}

                        <div  class="col-xs-6 col-sm-3 col-md-3 col-lg-3
                                    product-item
                                    brand-hideable brand-{$articoli[i]['ti_slug']|default: ""}
                                    gs-hideable gs-{$articoli[i]['gs_slug']|default: ""}
                                    size-hideable {section name="k" loop=$articoli[i]['movimenti']}size-{$articoli[i]['movimenti'][k]['mov_tari']} {/section}
                                    {if !empty($articoli[i]['discount'])}sale-active{else}sale-deactivated{/if}
                                " data-price="{if empty($articoli[i]['va_pre_dis'])}{$articoli[i]['va_pre_ven']}{else}{$articoli[i]['va_pre_dis']}{/if}">
                            {if !empty($articoli[i]['rif_web'])}
                                {assign var="rifWebString" value=$articoli[i]['rif_web']}
                                {assign var="path" value="---"|explode:$rifWebString}
                            {/if}

                            <div class="product-img {if empty($articoli[i]['img_gallery'][1]['imm_rpat']) || count($articoli[i]['img_gallery']) < 2 || strpos($articoli[i]['img_gallery'][1]['imm_rpat'], $path[2]|upper) != false}product-img-nohover{/if}" style="height: 361px; vertical-align: middle">
                                {if !empty($articoli[i]['discount'])}
                                    {if $articoli[i]['discount']['dis_tipo'] == "disc_perc" || $articoli[i]['discount']['dis_tipo'] == "coupon_per"}
                                        <div class="discount-container zoa-btn">-{$articoli[i]['discount']['dis_valu']}%</div>
                                    {/if}
                                    {if $articoli[i]['discount']['dis_tipo'] == "disc_impo" || $articoli[i]['discount']['dis_tipo'] == "coupon_imp"}
                                        <div class="discount-container zoa-btn">-{$articoli[i]['discount']['dis_valu']}&euro;</div>
                                    {/if}
                                {/if}
                                <a href="{base_url('shop/item')}/{$articoli[i]['ass_codi']}">
                                    {if !empty($articoli[i]['rif_web'])}

                                        {assign var="relative_path" value=""}
                                        {assign var="temp_path_1" value="PIC-UPLOAD/"|cat:$path[0]|cat:"/"|cat:($path[1]|upper)|cat:"/"|cat:$path[2]}
                                        {if file_exists($temp_path_1)}
                                            {assign var="relative_path" value=$temp_path_1}
                                        {/if}
                                        {assign var="temp_path_2" value="PIC-UPLOAD/"|cat:$articoli[i]['st_sigl']|cat:"/"|cat:($articoli[i]['ti_desc']|upper)|cat:"/"|cat:$path[2]}
                                        {if file_exists($temp_path_2)}
                                            {assign var="relative_path" value=$temp_path_2}
                                        {/if}
{*                                        {assign var="relative_path" value="PIC-UPLOAD/"|cat:$articoli[i]['st_sigl']|cat:"/"|cat:($articoli[i]['ti_desc']|upper)|cat:"/"|cat:$path[2]}*}


                                        {if file_exists($relative_path)}
                                            <img src="{if !empty($relative_path)}{base_url()}/{$relative_path}{/if}" alt="photo" class="img-responsive">
                                        {else}
                                            <img src="{base_url('assets-shop/img/product/no_img.png')}" alt="photo" class="img-responsive">
                                        {/if}
                                    {else}
                                        <img src="{base_url('assets-shop/img/product/no_img.png')}">
                                    {/if}

                                    {if count($articoli[i]['img_gallery']) > 1 && strpos($articoli[i]['img_gallery'][1]['imm_rpat'], $path[2]|upper) == false}
                                        <img src="{base_url("PIC-UPLOAD")}{$articoli[i]['img_gallery'][1]['imm_rpat']|upper}" alt="images" class="img-responsive image-effect">
                                    {/if}
                                </a>

{*                                    {section name="k" loop=$articoli[i]['img_gallery']}*}
{*                                                {if $articoli[i]['img_gallery'][k]['imm_rftb'] == "ARTICOLI"}*}
{*                                                    <img src="{base_url("pic-upload")}/{$articoli[i]['img_gallery'][k]['imm_rpat']}" alt="images" class="img-responsive image-effect">*}
{*                                                {else}*}
{*                                                {/if}*}
{*                                    {/section}*}

{*                                    <img src="{base_url('assets-shop/img/product/product_12.jpg')}" alt="" class="img-responsive image-effect"></a>*}
                                <div class="product-button-group ">
{*                                    <div class="shop-now">*}
{*                                        <a href="{base_url('shop/item')}/{$articoli[i]['md_codi']}{$articoli[i]['va_codi']}">{#shop_shop_now#} </a>*}
{*                                    </div>*}
{*                                    <div class="quick-view">*}
{*                                        <a href="#">{#shop_quick_view#} </a>*}
{*                                    </div>*}
                                    <div class="quick-view">
                                        <a href="{base_url('shop/item')}/{$articoli[i]['ass_codi']}">{#shop_quick_view#}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-info">
                                <div class="title-heart">
                                    <h3 class="product-title">
                                        <a href="{base_url('shop/item')}/{$articoli[i]['ass_codi']}">{$articoli[i]['md_desc']|truncate:18:"...":true}</a>
                                    </h3>
                                    <h3 class="product-cate padding-left-10">
                                        <a href="{base_url('shop/item')}/{$articoli[i]['ass_codi']}" style="color: grey !important;">{$articoli[i]['ti_desc']|truncate:18:"...":true}</a>
                                    </h3>
                                    <span class="zoa-icon-heart"></span>
                                </div>

                                <div class="short-desc">
                                    <ul class="product-desc">
                                        <li>{$articoli[i]['ass_dess']}</li>
                                    </ul>
                                </div>
                                <div class="product-price">
                                    {if empty($articoli[i]['va_pre_dis'])}
                                        <span>{$articoli[i]['va_pre_ven']|number_format:2:",":""}&euro;</span>
                                    {else}
                                        <del>{$articoli[i]['va_pre_ven']|number_format:2:",":""}&euro;</del>
                                        <span>{$articoli[i]['va_pre_dis']|number_format:2:",":""}&euro;</span>
                                    {/if}
                                </div>
                                <div class="color-group">
                                    {section name="k" loop=$articoli[i]['movimenti']}
                                        <span class="box-taglie">{$articoli[i]['movimenti'][k]['mov_tari']}</span>
                                    {/section}
                                </div>
                                <div class="product-bottom-group">
                                    <a href="#" class="zoa-btn zoa-addcart">
                                        <span class="zoa-icon-cart"></span> {#shop_add_to_cart#}
                                    </a>
                                    <a href="#" class="zoa-btn zoa-quickview">
                                        <span class="zoa-icon-quick-view"></span>
                                    </a>
                                    <a href="#" class="zoa-btn zoa-wishlist">
                                        <span class="zoa-icon-heart"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {/if}
                    {sectionelse}
                        <div class=" text-center">
                            {#no_results#}
                        </div>
                    {/section}


{*                    <div  class="  col-xs-6 col-sm-3 col-md-3 col-lg-3 product-item">*}
{*                        <div class="product-img">*}
{*                            <a href=""><img src="{base_url('assets-shop/img/product/product_4.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                                <img src="{base_url('assets-shop/img/product/product_3.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                            <div class="product-button-group ">*}
{*                                <div class="shop-now">*}
{*                                    <a href="#">{#shop_shop_now#} </a>*}
{*                                </div>*}
{*                                <div class="quick-view">*}
{*                                    <a href="#">{#shop_quick_view#} </a>*}
{*                                </div>*}
{*                            </div>*}
{*                        </div>*}
{*                        <div class="product-info ">*}
{*                            <div class="title-heart">*}
{*                                <h3 class="product-title">*}
{*                                    <a href="">Titolo del prodotto</a>*}
{*                                </h3>*}
{*                                <span class="zoa-icon-heart"></span>*}
{*                            </div>*}

{*                            <div class="short-desc">*}
{*                                <ul class="product-desc">*}
{*                                    <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                                    <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                                    <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                                </ul>*}
{*                            </div>*}
{*                            <div class="product-price">*}
{*                                <span>100,00&euro;</span>*}
{*                            </div>*}
{*                            <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                            </div>*}
{*                            <div class="product-bottom-group">*}
{*                                <a href="#" class="zoa-btn zoa-addcart">*}
{*                                    <span class="zoa-icon-cart"></span> ADD TO CART*}
{*                                </a>*}
{*                                <a href="#" class="zoa-btn zoa-quickview">*}
{*                                    <span class="zoa-icon-quick-view"></span>*}
{*                                </a>*}
{*                                <a href="#" class="zoa-btn zoa-wishlist">*}
{*                                    <span class="zoa-icon-heart"></span>*}
{*                                </a>*}
{*                            </div>*}
{*                        </div>*}
{*                    </div>*}
                </div>
            </div>
        </div>


{*        <div class=" text-center">*}
{*            <ul class="pagination">*}
{*                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>*}
{*                <li class="active"><a href="#">1</a></li>*}
{*                <li><a href="#">2</a></li>*}
{*                <li><a href="#">3</a></li>*}
{*                <li><a href="#">...</a></li>*}
{*                <li><a href="#">5</a></li>*}
{*                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>*}

{*            </ul>*}
{*        </div>*}


    </div>
</div>