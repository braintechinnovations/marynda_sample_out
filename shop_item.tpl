
<!-- Content -->

<div class=" container-fluid text-center breadcrumb-about">
    <h1>{$articoli->ass_dess}</h1>
    <ul class="breadcrumb">
        <li><a href="{base_url('/')}">{#shop_bread_home#}</a></li>
        <li><a href="{base_url('shop/grid')}">{#shop_bread_shop#}</a></li>
        {*        <li><a href="{base_url('shop/gs/')}/{$articoli->id_sgru_at}">{$articoli->gs_desc}</a></li>*}
        {*        <li><a href="{base_url('shop/gr')}/{$articoli->id_grup_at}">{$articoli->gr_desc}</a></li>*}
        <li><a href="#">{$articoli->gs_desc}</a></li>
        <li><a href="#">{$articoli->gr_desc}</a></li>
        <li class="active">{$articoli->ass_dess}</li>
    </ul>
</div>

<div class="container padding-top-30">
    <div class="single-product-detail v3 bd-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 zoa-width1">
                <div class="product-img-slide">
                    <div class="product-images">
                        {*                        <div class="ribbon zoa-sale"><span>-15%</span></div>*}


                        <div class="main-img js-product-slider-normal">

                            {assign var="rifWebString" value=$articoli->rif_web}
                            {assign var="path" value="---"|explode:$rifWebString}

                            <!-- Prima immagine: start -->
                            {if empty($articoli->rif_web)}
                                <a href="#" class="hover-images effect"><img src="{base_url('assets-shop/img/product/no_img.png')}" alt="Nessuna immagine" class="img-responsive"></a>
                            {else}
                                {assign var="relative_path" value=""}
                                {assign var="temp_path_2" value="PIC-UPLOAD/"|cat:$articoli->st_sigl|cat:"/"|cat:($articoli->ti_desc|upper)|cat:"/"|cat:$path[2]}
                                {if file_exists($temp_path_2)}
                                    {assign var="relative_path" value=$temp_path_2}
                                {/if}
                                {assign var="temp_path_1" value="PIC-UPLOAD/"|cat:$path[0]|cat:"/"|cat:($path[1]|upper)|cat:"/"|cat:$path[2]}
                                {if file_exists($temp_path_1)}
                                    {assign var="relative_path" value=$temp_path_1}
                                {/if}
{*                                {assign var="relative_path" value="PIC-UPLOAD/"|cat:$path[0]|cat:"/"|cat:($path[1]|upper)|cat:"/"|cat:$path[2]}*}
                                {if file_exists($relative_path)}
                                    <a href="#" class="hover-images effect"><img src="{if !empty($relative_path)}{base_url()}/{$relative_path}{/if}" alt="photo" class="img-responsive"></a>
                                {else}
                                    <a href="#" class="hover-images effect"><img src="{base_url('assets-shop/img/product/no_img.png')}" alt="photo" class="img-responsive"></a>
                                {/if}
                            {/if}
                            <!-- Prima immagine: end -->

                            <!-- Immagini successive: start -->
                            {section name="i" loop=$immagini}
                                {if $immagini[i]['imm_rftb'] == "ARTICOLI"}
                                    {assign var="relative_gal_big" value="PIC-UPLOAD"|cat:($immagini[i]['imm_rpat']|upper)}
                                {else}
                                    {assign var="relative_gal_big" value="PIC-UPLOAD"|cat:$articoli->st_sigl|cat:"/"|cat:($articoli->ti_desc|upper)|cat:"/"|cat:$immagini[i]['imm_rpat']}
                                {/if}
                                {if file_exists($relative_gal_big)}
                                <div class="product-col">
                                    <div class="img">
                                            <img src="{base_url()}/{$relative_gal_big}" alt="images" class="img-responsive">
                                        <div class="bg"></div>
                                    </div>
                                </div>
                                {/if}
                            {/section}
                            <!-- Immagini successive: end -->

                        </div>
                    </div>
                    <div class="multiple-img-list js-click-product-normal">
                        <!-- Mini preview prima immagine: start -->
                        <div class="product-col">
                            <div class="img active">
                                {if empty($articoli->rif_web)}
                                    <img src="{base_url('assets-shop/img/product/no_img.png')}" alt="Nessuna immagine" alt="photo" class="img-responsive">
                                {else}
                                    {if file_exists($relative_path)}
                                        <a href="#" class="hover-images effect"><img src="{if !empty($relative_path)}{base_url()}/{$relative_path}{/if}" alt="photo" class="img-responsive"></a>
                                    {else}
                                        <a href="#" class="hover-images effect"><img src="{base_url('assets-shop/img/product/no_img.png')}" alt="photo" class="img-responsive"></a>
                                    {/if}
                                {/if}
                                <div class="bg"></div>
                            </div>
                        </div>
                        <!-- Mini preview prima immagine: end -->
                        {section name="i" loop=$immagini}
                            <div class="product-col">
                                <div class="img">
                                    {if $immagini[i]['imm_rftb'] == "ARTICOLI"}
                                        {assign var="relative_gal" value="PIC-UPLOAD"|cat:($immagini[i]['imm_rpat']|upper)}
                                    {else}
                                        {assign var="relative_gal" value="PIC-UPLOAD"|cat:$articoli->st_sigl|cat:"/"|cat:($articoli->ti_desc|upper)|cat:"/"|cat:$immagini[i]['imm_rpat']}
                                    {/if}

                                    {if file_exists($relative_gal)}
                                        <img src="{base_url("PIC-UPLOAD")}/{$immagini[i]['imm_rpat']|upper}" alt="images" class="img-responsive">
                                    {/if}
                                    <div class="bg"></div>
                                </div>
                            </div>
                        {/section}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 zoa-width2">
                <div class="single-product-info product-info product-grid-v2">
                    {*                    <div class="flex product-rating">*}
                    {*                        <div class="group-star">*}
                    {*                            <span class="star star-5"></span>*}
                    {*                            <span class="star star-4"></span>*}
                    {*                            <span class="star star-3"></span>*}
                    {*                            <span class="star star-2"></span>*}
                    {*                            <span class="star star-1"></span>*}
                    {*                        </div>*}
                    {*                        <div class="number-rating">( {$recensioni|count} {#item_reviews#|lower} )</div>*}
                    {*                    </div>*}
                    <h3 class="product-title"><a href="#">{$articoli->ass_dess}</a></h3>
                    <div class="product-price">
                        {if empty($articoli->va_pre_dis)}
                            <span>{$articoli->va_pre_ven|number_format:2:",":""}&euro;</span>
                        {else}
                            <del>{$articoli->va_pre_ven|number_format:2:",":""}&euro;</del>
                            <span>{$articoli->va_pre_dis|number_format:2:",":""}&euro;</span>
                        {/if}
                    </div>

                    <div class="product-countdown">
                        {if !empty($articoli->discount)}
                            {if $articoli->discount['dis_tipo'] == "disc_perc" || $articoli->discount['dis_tipo'] == "coupon_per"}
                                <span> {#item_discount#} del <strong>{$articoli->discount['dis_valu']}%</strong></span>
                            {/if}
                        {/if}
                        {if !empty($articoli->discount)}
                            {if $articoli->discount['dis_tipo'] == "disc_impo" || $articoli->discount['dis_tipo'] == "coupon_imp"}
                                <span> {#item_discount#} di <strong>{$articoli->discount['dis_valu']}&euro;</strong></span>
                            {/if}
                        {/if}
                    </div>
                    <div class="product-countdown ">
                        <p>{$articoli->ass_desc}</p>
                    </div>
                    <div class="product-countdown ">
                        <p>{#item_color#}: <strong>{$articoli->cr_desc}</strong></p>
                    </div>
{*                                        <div class="product-size">*}
{*                                            <div class="size-group">*}
{*                                                <label>Color </label>*}
{*                                                <select class="single-option-selector" data-option="option1" id="productSelect-option-0">*}
{*                                                    <option value="GREY">GREY</option>*}
{*                                                    <option value="BLACK">BLACK</option>*}
{*                                                    <option value="YELLOW">YELLOW</option>*}
{*                                                    <option value="WHITE">WHITE</option>*}

{*                                                </select>*}
{*                                            </div>*}
{*                                        </div>*}
                    <div class="product-size">
                        <div class="size-group">
                            <label>{#item_size#} </label>
                            <select class="single-option-selector" data-option="option4" name="size_select">

                                <option value="" selected>{#item_select_size#}</option>
                                {foreach from=$movimenti item=mov}
                                    {if $mov->mov_esie != 0}
                                        <option value="{$mov->mov_tari}" {if $mov->mov_esie == 0}style="color: red;" disabled{/if}>{$mov->mov_tari} - disp. {$mov->mov_esie}</option>
                                    {/if}
                                    {foreachelse}
                                    <option value="">{#item_not_availab#}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="single-product-button-group">
                        <div class="flex align-items-center element-button" data-model="{$articoli->md_codi}" data-variation="{$articoli->va_codi}">
                            <div class="zoa-qtt">
                                <button type="button" class="quantity-left-minus btn btn-number js-minus" data-type="minus" data-field="">
                                </button>
                                <input type="text" value="1" class="product_quantity_number js-number" name="qty_input">
                                <button type="button" class="quantity-right-plus btn btn-number js-plus" data-type="plus" data-field="">
                                </button>
                            </div>
                            <button type="button" class="zoa-btn zoa-addcart shop-item-addcart">
                                <i class="zoa-icon-cart"></i>{#shop_add_to_cart_short#}
                            </button>
                            <button type="button" class="zoa-btn zoa-wishlist shop-item-addwishlist">
                                <i class="zoa-icon-heart"></i>
                            </button>
                        </div>

                    </div>


                    <div class="panel-group" id="accordion">
                        {foreach from=$pages key=k item=v}
                            <div class="panel panel-default {if $k == 0}panel-first{/if}">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{$k}">{$pages[$k]['pag_titl']}</a>
                                    </h4>
                                </div>
                                <div id="collapse{$k}" class="panel-collapse collapse {if $k == 0}in{/if}">
                                    <div class="panel-body">{$pages[$k]['pag_resu']}</div>
                                </div>
                            </div>
                        {/foreach}
                    </div>

                    <div class="product-social">
                        <label>{#share_style#}</label>
                        <div class="social">
                            <a href=""><i class="fa fa-facebook"></i></a>
                            <a href=""><i class="fa fa-instagram"></i></a>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 single-tab ">
                <ul class="nav nav-pills text-center bd-top bd-bottom">
                    <li class="active"><a data-toggle="pill" href="#description">{#item_details#|upper}</a></li>
                    {*                    <li ><a data-toggle="pill" href="#all">ADDITIONAL</a></li>*}
                    {*                    <li><a data-toggle="pill" href="#all">INFORMATION</a></li>*}
                    <li><a data-toggle="pill" href="#all">{#item_reviews#|upper} ({$recensioni|count})</a></li>
                </ul>
                <div class="tab-content">
                    <div id="description" class="tab-pane fade active in">
                        <p>{$articoli->ass_desc}</p>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="all" class="tab-pane">
                        {foreach from=$recensioni item=rec}
                            <p>
                                <strong>{$rec['rec_titl']}</strong><br />
                                {$rec['rec_comm']}
                            </p>
                            <hr />
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container container-content padding-top-50">
    <div class="text-h1 text-center">
        <h3 class="related-title text-center">{#shop_may_be_like#|upper}</h3>
    </div>
    <div class="row padding-top-30">

        {section name="i" loop=$collegati}
            {if $collegati[i]['quant']|intval > 0}

                <div class="col-xs-6  col-md-2 col-lg-2 width20 product-item">
                    {if !empty($collegati[i]['rif_web'])}
                        {assign var="rifWebString" value=$collegati[i]['rif_web']}
                        {assign var="path" value="---"|explode:$rifWebString}
                    {/if}

                    <div class="product-img {if count($collegati[i]['img_gallery']) < 2 || !empty($collegati[i]['img_gallery'][1]['imm_rpat']) || strpos($collegati[i]['img_gallery'][1]['imm_rpat'], $path[2]|upper) == false}product-img-nohover{/if}" style="height: 361px; vertical-align: middle">
                        {if !empty($collegati[i]['discount'])}
                            {if $collegati[i]['discount']['dis_tipo'] == "disc_perc" || $collegati[i]['discount']['dis_tipo'] == "coupon_per"}
                                <div class="discount-container zoa-btn">-{$collegati[i]['discount']['dis_valu']}%</div>
                            {/if}
                            {if $collegati[i]['discount']['dis_tipo'] == "disc_impo" || $collegati[i]['discount']['dis_tipo'] == "coupon_imp"}
                                <div class="discount-container zoa-btn">-{$collegati[i]['discount']['dis_valu']}&euro;</div>
                            {/if}
                        {/if}
                        <a href="{base_url('shop/item')}/{$collegati[i]['md_codi']}{$collegati[i]['va_codi']}">
                            {if !empty($collegati[i]['rif_web'])}
                                <img src="{if !empty($path[0])}{base_url("PIC-UPLOAD")}/{$path[0]|upper}/{$path[1]|upper}/{$path[2]|upper}{/if}" alt="" class="img-responsive wp-post-image">
                            {else}
                                <img src="{base_url('assets-shop/img/product/no_img.png')}">
                            {/if}

                            {if count($collegati[i]['img_gallery']) > 1 && strpos($collegati[i]['img_gallery'][1]['imm_rpat'], $path[2]|upper) == false}
                                <img src="{base_url("PIC-UPLOAD")}{$collegati[i]['img_gallery'][1]['imm_rpat']|upper}" alt="images" class="img-responsive image-effect">
                            {/if}
                        </a>
                        <div class="product-button-group ">
                            {*                            <div class="shop-now">*}
                            {*                                <a href="{base_url('shop/item')}/{$collegati[i]['md_codi']}{$collegati[i]['va_codi']}">{#shop_shop_now#} </a>*}
                            {*                            </div>*}
                            <div class="quick-view">
                                <a href="{base_url('shop/item')}/{$collegati[i]['md_codi']}{$collegati[i]['va_codi']}">{#shop_shop_now#} </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <div class="title-heart">
                            <h3 class="product-title">
                                <a href="{base_url('shop/item')}/{$collegati[i]['md_codi']}{$collegati[i]['va_codi']}">{$collegati[i]['md_desc']}</a>
                            </h3>
                            <h3 class="product-cate padding-left-10">
                                <a href="{base_url('shop/item')}/{$collegati[i]['md_codi']}{$collegati[i]['va_codi']}" style="color: grey !important;">{$collegati[i]['ti_desc']}</a>
                            </h3>
                            <span class="zoa-icon-heart"></span>
                        </div>

                        <div class="short-desc">
                            <ul class="product-desc">
                                <li>{$collegati[i]['ass_dess']}</li>
                            </ul>
                        </div>
                        <div class="product-price">
                            {if empty($collegati[i]['va_pre_dis'])}
                                <span>{$collegati[i]['va_pre_ven']|number_format:2:",":""}&euro;</span>
                            {else}
                                <del>{$collegati[i]['va_pre_ven']|number_format:2:",":""}&euro;</del>
                                <span>{$collegati[i]['va_pre_dis']|number_format:2:",":""}&euro;</span>
                            {/if}
                        </div>
                    </div>
                </div>
            {/if}
            {sectionelse}
            <div class=" text-center">
                {#no_results#}
            </div>
        {/section}
    </div>
</div>

{*<div class="container container-content">*}
{*    <div class="product-related ">*}
{*        <h3 class="related-title text-center">{#shop_may_be_like#|upper}</h3>*}
{*        <div class="owl-carousel owl-theme owl-cate v2 js-owl-cate">*}
{*            <div  class="  product-item">*}
{*                <div class="product-img">*}
{*                    <a href=""><img src="{base_url('assets-shop/img/product/product_1.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                        <img src="{base_url('assets-shop/img/product/product_2.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                    <div class="product-button-group ">*}
{*                        <div class="shop-now">*}
{*                            <a href="#">{#shop_shop_now#} </a>*}
{*                        </div>*}
{*                        <div class="quick-view">*}
{*                            <a href="#">{#shop_quick_view#} </a>*}
{*                        </div>*}
{*                    </div>*}
{*                </div>*}
{*                <div class="product-info ">*}
{*                    <div class="title-heart">*}
{*                        <h3 class="product-title">*}
{*                            <a href="">Titolo del prodotto</a>*}
{*                        </h3>*}
{*                        <span class="zoa-icon-heart"></span>*}
{*                    </div>*}

{*                    <div class="short-desc">*}
{*                        <ul class="product-desc">*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                        </ul>*}
{*                    </div>*}
{*                    <div class="product-price">*}
{*                        <span>100,00&euro;</span>*}
{*                    </div>*}
{*                    <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*            <div  class="  product-item">*}
{*                <div class="product-img">*}
{*                    <a href=""><img src="{base_url('assets-shop/img/product/product_2.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                        <img src="{base_url('assets-shop/img/product/product_3.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                    <div class="product-button-group ">*}
{*                        <div class="shop-now">*}
{*                            <a href="#">{#shop_shop_now#} </a>*}
{*                        </div>*}
{*                        <div class="quick-view">*}
{*                            <a href="#">{#shop_quick_view#} </a>*}
{*                        </div>*}
{*                    </div>*}
{*                </div>*}
{*                <div class="product-info ">*}
{*                    <div class="title-heart">*}
{*                        <h3 class="product-title">*}
{*                            <a href="">Titolo del prodotto</a>*}
{*                        </h3>*}
{*                        <span class="zoa-icon-heart"></span>*}
{*                    </div>*}

{*                    <div class="short-desc">*}
{*                        <ul class="product-desc">*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                        </ul>*}
{*                    </div>*}
{*                    <div class="product-price">*}
{*                        <span>100,00&euro;</span>*}
{*                    </div>*}
{*                    <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*            <div  class="  product-item">*}
{*                <div class="product-img">*}
{*                    <a href=""><img src="{base_url('assets-shop/img/product/product_3.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                        <img src="{base_url('assets-shop/img/product/product_4.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                    <div class="product-button-group ">*}
{*                        <div class="shop-now">*}
{*                            <a href="#">{#shop_shop_now#} </a>*}
{*                        </div>*}
{*                        <div class="quick-view">*}
{*                            <a href="#">{#shop_quick_view#} </a>*}
{*                        </div>*}
{*                    </div>*}
{*                </div>*}
{*                <div class="product-info ">*}
{*                    <div class="title-heart">*}
{*                        <h3 class="product-title">*}
{*                            <a href="">Titolo del prodotto</a>*}
{*                        </h3>*}
{*                        <span class="zoa-icon-heart"></span>*}
{*                    </div>*}

{*                    <div class="short-desc">*}
{*                        <ul class="product-desc">*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                        </ul>*}
{*                    </div>*}
{*                    <div class="product-price">*}
{*                        <span>100,00&euro;</span>*}
{*                    </div>*}
{*                    <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*            <div  class="  product-item">*}
{*                <div class="product-img">*}
{*                    <a href=""><img src="{base_url('assets-shop/img/product/product_4.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                        <img src="{base_url('assets-shop/img/product/product_5.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                    <div class="product-button-group ">*}
{*                        <div class="shop-now">*}
{*                            <a href="#">{#shop_shop_now#} </a>*}
{*                        </div>*}
{*                        <div class="quick-view">*}
{*                            <a href="#">{#shop_quick_view#} </a>*}
{*                        </div>*}
{*                    </div>*}
{*                </div>*}
{*                <div class="product-info ">*}
{*                    <div class="title-heart">*}
{*                        <h3 class="product-title">*}
{*                            <a href="">Titolo del prodotto</a>*}
{*                        </h3>*}
{*                        <span class="zoa-icon-heart"></span>*}
{*                    </div>*}

{*                    <div class="short-desc">*}
{*                        <ul class="product-desc">*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                        </ul>*}
{*                    </div>*}
{*                    <div class="product-price">*}
{*                        <span>100,00&euro;</span>*}
{*                    </div>*}
{*                    <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*            <div  class="  product-item">*}
{*                <div class="product-img">*}
{*                    <a href=""><img src="{base_url('assets-shop/img/product/product_6.jpg')}" alt="" class="img-responsive wp-post-image">*}
{*                        <img src="{base_url('assets-shop/img/product/product_7.jpg')}" alt="" class="img-responsive image-effect"></a>*}
{*                    <div class="product-button-group ">*}
{*                        <div class="shop-now">*}
{*                            <a href="#">{#shop_shop_now#} </a>*}
{*                        </div>*}
{*                        <div class="quick-view">*}
{*                            <a href="#">{#shop_quick_view#} </a>*}
{*                        </div>*}
{*                    </div>*}
{*                </div>*}
{*                <div class="product-info ">*}
{*                    <div class="title-heart">*}
{*                        <h3 class="product-title">*}
{*                            <a href="">Titolo del prodotto</a>*}
{*                        </h3>*}
{*                        <span class="zoa-icon-heart"></span>*}
{*                    </div>*}

{*                    <div class="short-desc">*}
{*                        <ul class="product-desc">*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                            <li>Dettaglio articolo dettaglio articolo dettaglio articolo dettaglio articolo.</li>*}
{*                        </ul>*}
{*                    </div>*}
{*                    <div class="product-price">*}
{*                        <span>100,00&euro;</span>*}
{*                    </div>*}
{*                    <div class="color-group"><!--*}
{*                                <a href="#" class="circle gray"></a>*}
{*                                <a href="#" class="circle black active"></a>*}
{*                                <a href="#" class="circle yellow "></a>*}
{*                                <a href="#" class="circle white"></a>-->*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}

{*        </div>*}
{*    </div>*}
{*</div>*}