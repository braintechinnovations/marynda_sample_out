<!DOCTYPE html>
<html lang="{#meta_lang#}">

<head>
    {if !empty($smarty.env.CI_ENVIRONMENT) && $smarty.env.CI_ENVIRONMENT == 'production'}
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130489058-1"></script>
        <script>
            {literal}
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-130489058-1');
            {/literal}
        </script>

        <!-- Start Facebook Verification Code -->
        <meta name="facebook-domain-verification" content="5ecis9fvvp5aovygj2ksqnnx04pg33" />
        <meta name="facebook-domain-verification" content="9xv3lp1z53ifx2wuarqnufdb13ioxr" />
        <!-- End Facebook Verification Code -->

        <!-- Facebook Pixel Code -->
        <script>
            {literal}
            !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '679148802179651');
                fbq('track', 'PageView');
            {/literal}
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=679148802179651&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Start MailChimp Code -->
        {literal}
        <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/986a1f4847007632cc1e20999/e754898e30dca79ef1788421b.js");</script>
        {/literal}
        <!-- End MailChimp Code -->
    {else}
        <!-- No index please -->

        <!-- Facebook Pixel Code -->
        <script>
            {literal}
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '');
            fbq('track', 'PageView');
            {/literal}
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=0000000000&ev=PageView&noscript=1"/>
        </noscript>

        <meta name="robots" content="noindex">
    {/if}

    {if !empty($ogDataArticle)}
        <!-- OG Article Facebook: start -->
        {if !empty($ogDataArticle['url'])}<meta property="og:url" content="{$ogDataArticle['url']}" />{/if}
        {if !empty($ogDataArticle['type'])}<meta property="og:type" content="{$ogDataArticle['type']}" />{/if}
        {if !empty($ogDataArticle['title'])}<meta property="og:title" content="{$ogDataArticle['title']}" />{/if}
        {if !empty($ogDataArticle['description'])}<meta property="og:description" content="{$ogDataArticle['description']}" />{/if}
        {if !empty($ogDataArticle['image'])}<meta property="og:image" content="{$ogDataArticle['image']}" />{/if}
        <!-- OG Article Facebook: end -->
    {/if}

    {if !empty($ogDataShopItem)}
        <!-- OG Shop Item Facebook: start -->
{*        {if !empty($ogDataShopItem['retailer_item_id'])}<meta property="product:id" content="{$ogDataShopItem['retailer_item_id']}">{/if}*}
        {if !empty($ogDataShopItem['title'])}<meta property="og:title" content="{$ogDataShopItem['title']}">{/if}
        {if !empty($ogDataShopItem['description'])}<meta property="og:description" content="{$ogDataShopItem['description']}">{/if}
        {if !empty($ogDataShopItem['url'])}<meta property="og:url" content="{$ogDataShopItem['url']}">{/if}
        {if !empty($ogDataShopItem['image'])}<meta property="og:image" content="{$ogDataShopItem['image']}">{/if}
        {if !empty($ogDataShopItem['brand'])}<meta property="product:brand" content="{$ogDataShopItem['brand']}">{/if}
        {if !empty($ogDataShopItem['availability'])}<meta property="product:availability" content="{$ogDataShopItem['availability']}">{/if}
        {if !empty($ogDataShopItem['condition'])}<meta property="product:condition" content="{$ogDataShopItem['condition']}">{/if}
        {if !empty($ogDataShopItem['amount'])}<meta property="product:price:amount" content="{$ogDataShopItem['amount']}">{/if}
        {if !empty($ogDataShopItem['amount'])}<meta property="product:price" content="{$ogDataShopItem['amount']}">{/if}
        {if !empty($ogDataShopItem['currency'])}<meta property="product:price:currency" content="{$ogDataShopItem['currency']}">{/if}
        {if !empty($ogDataShopItem['retailer_item_id'])}<meta property="product:retailer_item_id" content="{$ogDataShopItem['retailer_item_id']}">{/if}
        {if !empty($ogDataShopItem['item_group_id'])}<meta property="product:item_group_id" content="{$ogDataShopItem['item_group_id']}">{/if}
        <!-- OG Shop Item Facebook: end -->
    {/if}

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    {if empty($smarty.server.PATH_INFO)}
        <meta name="Description" CONTENT="{#seo_index_description#}">
    {/if}

    {if !empty($smarty.server.PATH_INFO)}
        {if $smarty.server.PATH_INFO|strstr:'/donna'}<meta name="Description" CONTENT="{#seo_donna_description#}">{/if}
        {if $smarty.server.PATH_INFO|strstr:'/uomo'}<meta name="Description" CONTENT="{#seo_uomo_description#}">{/if}
        {if $smarty.server.PATH_INFO|strstr:'/food'}<meta name="Description" CONTENT="{#seofooda_description#}">{/if}

    {/if}

    <title>{$title} | {#portal_title#}</title>

    <link rel="shortcut icon" href="{base_url('assets-shop/img/favicon.png')}" type="image/png">
{*    <link rel="stylesheet" href="{base_url('assets-shop/bootstrap-4.13/css/bootstrap-grid.css')}">*}
    <link rel="stylesheet" href="{base_url('assets-shop/css/owl.carousel.min.css')}">
    <link rel="stylesheet" href="{base_url('assets-shop/css/slick.css')}">
    <link rel="stylesheet" href="{base_url('assets-shop/css/slick-theme.css')}">
    <link rel="stylesheet" href="{base_url('assets-shop/css/owl.theme.default.min.css')}">
<<<<<<< Updated upstream
    <link rel="stylesheet" href="{base_url('assets-shop/css/jquery-ui.css')}">
=======
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
>>>>>>> Stashed changes
    <link rel="stylesheet" type="text/css" href="{base_url('assets-shop/css/styles.css')}">
    <link rel="stylesheet" type="text/css" href="{base_url('assets-shop/css/font-awesome-5.15.4/css/all.css')}">
    <link rel="stylesheet" type="text/css" href="{base_url('assets-shop/css/custom_shop.css')}?v={$smarty.now}">
    <link rel="stylesheet" type="text/css" href="{base_url('assets/plugins/custom/whatsapp/whatsapp-chat-support.css')}">
</head>

<body>

